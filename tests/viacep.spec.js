const { test } = require('@japa/runner')

test.group('ViaCEPProvider', () => {
  test('Teste de chamada do método sayViaCEP(name)', async ({ assert }) => {
    const ViaCEP = require('../src/ViaCEP')
    const viaCEP = new ViaCEP()

    const response = await viaCEP.getAddress('01001000')
    const compare = {
      cep: '01001-000',
      logradouro: 'Praça da Sé',
      complemento: 'lado ímpar',
      bairro: 'Sé',
      localidade: 'São Paulo',
      uf: 'SP',
      ibge: '3550308',
      gia: '1004',
      ddd: '11',
      siafi: '7107'
    }

    assert.equal(response, compare)
  })
})
