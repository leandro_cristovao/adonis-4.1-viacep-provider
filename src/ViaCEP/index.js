const fetch = require('node-fetch')

class ViaCEP {
  constructor(Config) {
    this.Config = Config
  }

  async getAddress(cep) {

    //
    // const response = await fetch(`${this.Config.get('viacep.url')}/${cep}/json`)
    const response = await fetch(`https://viacep.com.br/ws/${cep}/json/`)
    const data = await response.json();
    return data

    // console.log(response.ok);
    // console.log(response.status);
    // console.log(response.statusText);
    // console.log(response.headers.raw());
    // console.log(response.headers.get('content-type'));
  }
}

module.exports = ViaCEP
