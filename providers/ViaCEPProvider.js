const { ServiceProvider } = require('@adonisjs/fold')

class ViaCEPProvider extends ServiceProvider {
  register() {
    this.app.singleton('Adonis/Addons/ViaCEP', () => {
      const Config = this.app.use('Adonis/Src/Config')
      return new (require('../src/ViaCEP'))(Config)
    })
  }
}

module.exports = ViaCEPProvider
